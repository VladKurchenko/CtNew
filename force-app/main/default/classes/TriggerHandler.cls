public interface TriggerHandler {
    void beforeInsert(List<SObject> newRecords);
    void afterUpdate(List<SObject> oldRecords, List<SObject> newRecords, Map<ID, SObject> oldRecordMap, Map<ID, SObject> newRecordMap);
}