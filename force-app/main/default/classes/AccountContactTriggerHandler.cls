public without sharing class AccountContactTriggerHandler implements TriggerHandler {
    public void beforeInsert(List<AccountContact__c> references) {
        Map<Id, List<AccountContact__c>> referencesByContactIds = new Map<Id, List<AccountContact__c>>();
        for (AccountContact__c ref: references) {
            if (referencesByContactIds.containsKey(ref.Contact__c)) {
                referencesByContactIds.get(ref.Contact__c).add(ref);
            }
            else {
                referencesByContactIds.put(ref.Contact__c,new List<AccountContact__c>{ref});
            }
        }

        List<AccountContact__c> existingReferences = [SELECT Id, Name, Contact__c FROM AccountContact__c where Contact__c in: referencesByContactIds.keySet()];

        for (AccountContact__c existingRef : existingReferences) {
            referencesByContactIds.remove(existingRef.Contact__c);
        }
        
        //set is primary to the first references of each contact  
        for (List<AccountContact__c> newReferences : referencesByContactIds.values()) {
            newReferences.get(0).IsPrimary__c = true;
        }
    }  
          
    public void afterUpdate(List<AccountContact__c> oldReferences, List<AccountContact__c> newReferences, Map<ID, SObject> oldReferencesMap, Map<ID, SObject> newReferencesMap) {
        Set<Id> contactsToSetPrimaryFalse = new Set<Id>();
        Set<Id> contactsToSetPrimaryTrue = new Set<Id>();
        AccountContact__c oldReference;
        for (accountContact__c newReference : newReferences) {
           oldReference = (AccountContact__c)oldReferencesMap.get(newReference.Id); 
           if (newReference.IsPrimary__c != oldReference.IsPrimary__c) {
               if (newReference.IsPrimary__c) {
                    contactsToSetPrimaryFalse.add(newReference.Contact__c);     
               }

               if (oldReference.IsPrimary__c) {
                    contactsToSetPrimaryTrue.add(oldReference.Contact__c); 
               }
           }
        } 
        //Some changes
        //Trying to test git
        List<AccountContact__c> referencesForUpdate = new List<AccountContact__c>();
        for (Contact Cont : [SELECT id,(SELECT id From AccountContact__r where isPrimary__c = false and id not in :newReferences order by CreatedDate desc LIMIT 1) From Contact where id in:contactsToSetPrimaryTrue]) {
            for (AccountContact__c Reference : Cont.AccountContact__r) {
                referencesForUpdate.add(new accountContact__c(id=Reference.id,isPrimary__c = true));   
            }
        }
        for (Contact Cont : [SELECT id,(SELECT id From AccountContact__r where isPrimary__c = true and id not in :newReferences order by CreatedDate desc LIMIT 1) From Contact where id in:contactsToSetPrimaryFalse]) {
            for (AccountContact__c Reference : Cont.AccountContact__r) {
                referencesForUpdate.add(new accountContact__c(id=Reference.id,isPrimary__c = false));   
            }
        }
        //Update 
        update referencesForUpdate;
    }
}