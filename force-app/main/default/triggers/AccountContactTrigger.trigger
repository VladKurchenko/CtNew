trigger AccountContactTrigger on AccountContact__c (before insert, after update) {
    
    TriggerHandler handler = new AccountContactTriggerHandler();
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            handler.beforeInsert(Trigger.new);
        }
    }
    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            handler.afterUpdate(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap);
        }
    }   
}